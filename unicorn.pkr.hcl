packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "ubuntu" {
  ami_name      = "strongdm-unicorn-worker"
  instance_type = "t2.micro"
  region        = "us-east-2"
  vpc_id        = "vpc-0e9e60c71359d30d5"
  subnet_id     = "subnet-031e935a801cd263e"
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-focal-20.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
  ssh_username = "ubuntu"
}

build {
  name = "strongdm"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]
  provisioner "shell" {
    script = "unicorn_build.sh"
  }
  provisioner "file" {
    source      = "unicorn.service"
    destination = "/tmp/unicorn.service"
  }
  provisioner "shell" {
    inline = [
      "sudo cp /tmp/unicorn.service /etc/systemd/system/unicorn.service",
      "sudo ln -s /etc/systemd/system/unicorn.service /etc/systemd/system/multi-user.target.wants/unicorn.service"
    ]
  }

}

