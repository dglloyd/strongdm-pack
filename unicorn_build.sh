echo "Update ubuntu"
sudo rm -rf /var/lib/apt/lists/*
sudo apt-get update -qq && sudo apt-get dist-upgrade --yes

echo "Downloading go 17.2"
curl -sS https://dl.google.com/go/go1.17.2.linux-amd64.tar.gz -o /tmp/go1.17.2.linux-amd64.tar.gz

echo "Extracting go"
sudo tar -C /usr/local -xzf /tmp/go1.17.2.linux-amd64.tar.gz

sudo echo "export PATH=$PATH:/usr/local/go/bin" |sudo tee -a /etc/profile
export PATH=$PATH:/usr/local/go/bin
go version

echo "Install unicorn package"
go install cirello.io/unicorn@v1.0.0

echo "Done"
